# desafio-log-parser-code

## Installing Ruby with rvm

Debian GNU/Linux and Ubuntu use the apt package manager. You can use it as follows:

first follow this tutorial to install rvm and ruby on linux:

https://github.com/rvm/ubuntu_rvm

after that 
## Clone this repository using SSH 

Enter the terminal go to the directry you want and type:

```
$ git clone git@gitlab.com:lorena.silva1/desafio-log-parser-code.git
```


## Installing Rspec and Bundle in your local respository 

- Open in terminal your repository directory and run:

```
$Bundle install
```
After that run in your project:

```
$ rspect --init
```
## Run main_parser.rb

Open terminal and type

```
$irb
```
then press enter. After that type:

```
$ ruby parser.rb
```
## run tests with bundle

to run any test in your projets using bundles, type in terminal in your project directory:

```
$ bundle exec rspec
```
