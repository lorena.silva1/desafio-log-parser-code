class Parser
  def initialize(file_path)  # Construtor da classe
    @file_path = file_path
    raise "not found." unless File.file?(@file_path)
  end

  def show_file_first_line     # Metodo que abre e lê o arquivo games.log e escreve a primeira linha do arquivo
    File.open(@file_path, "r") do |file|
      return file.readline
    end
  end

  def to_json
    file_path = {lines: num_of_lines_in_file}
    file_path.to_json
  end  

  private
  def num_of_lines_in_file
    File.readlines(@file_path).size
  end
end
  