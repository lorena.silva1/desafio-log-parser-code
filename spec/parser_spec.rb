require_relative '../parser.rb'

describe Parser do
  describe "#show_file_first_line" do
    context "when the file does exists" do
      it "open the file" do
        log = Parser.new("spec/fixtures/games_test.log")
        expect("  0:00 ------------------------------------------------------------")
      end
    end
  end
    context "when the file does not exist" do
      it "give an error message" do
        expect{Parser.new("games_ttest.log")}.to raise_error("not found.")
      end
    end

describe "#num_of_lines_in_a_file" do
  it "If it has the same number of lines" do
    log = Parser.new("spec/fixtures/games_test.log")
      expect(log.num_of_lines_in_file).to eq(11) 
    end
  end
end
            
